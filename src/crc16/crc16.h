#ifndef STELLAR_SDK_CPP_MC_CRC16_H
#define STELLAR_SDK_CPP_MC_CRC16_H

#include <stdlib.h>

typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
//typedef long int32_t;
//typedef long long int64_t;
//typedef unsigned long uint32_t;
//typedef unsigned long long uint64_t;

typedef uint16_t bit_order_16(uint16_t value);
typedef uint8_t bit_order_8(uint8_t value);

uint16_t crc16(const uint8_t *data, uint16_t size);

uint16_t crc16ccitt(uint8_t const *message, int nBytes);
uint16_t crc16ccitt_xmodem(uint8_t const *message, int nBytes);
uint16_t crc16ccitt_kermit(uint8_t const *message, int nBytes);
uint16_t crc16ccitt_1d0f(uint8_t const *message, int nBytes);
uint16_t crc16ibm(uint8_t const *message, int nBytes);

#endif //STELLAR_SDK_CPP_MC_CRC16_H
