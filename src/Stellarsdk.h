#ifndef STELLAR_SDK_CPP_MC_STELLARSDK_H
#define STELLAR_SDK_CPP_MC_STELLARSDK_H

#include "Arduino.h"

#include "Keypair.h"

class StellarSDK {
public:
    StellarSDK();

    Keypair keypair;

    void createPaymentOp();
};

#endif //STELLAR_SDK_CPP_MC_STELLARSDK_H
