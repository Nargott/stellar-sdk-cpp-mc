#ifndef STELLAR_SDK_CPP_MC_KEYPAIR_H
#define STELLAR_SDK_CPP_MC_KEYPAIR_H

#define SEED_SIZE 32
#define PUBKEY_SIZE 32
#define PRIVKEY_SIZE 64
#define SIGN_SIZE 64
#define SECRET_SIZE 56
#define PUBLIC_SIZE 56

#include "Arduino.h"
#include "base32/Base32.h"

typedef enum {
    // version bytes - 5 bits only
    STRKEY_PUBKEY_ED25519 = 6 << 3, // 'G'
    STRKEY_SEED_ED25519 = 18 << 3,  // 'S'
    STRKEY_PRE_AUTH_TX = 19 << 3,   // 'T',
    STRKEY_HASH_X = 23 << 3         // 'X'
} StrKeyVersionByte;

class Keypair {
public:
    Base32 base32;

    unsigned char rawSeed[SEED_SIZE];
    unsigned char rawPublicKey[PUBKEY_SIZE];
    unsigned char rawPrivateKey[PRIVKEY_SIZE];
    unsigned char rawSignature[SIGN_SIZE];
    unsigned char publicKey[PRIVKEY_SIZE];
    unsigned char secret[SECRET_SIZE];

    String publicKeyStr;

    void randomSeed();
    void create();
    void makePublicKey();
    void makeSecret();
    void randomKeypair();
    void sign(const unsigned char *message, size_t message_len);
    int verify(const unsigned char *message, size_t message_len);
    void addScalar(const unsigned char *scalar);
    void keyExchange(unsigned char *shared_secret);


private:
    static int constTimeEqual(const unsigned char *x, const unsigned char *y);
    void encodeCheck(byte versionByte, unsigned char *data, size_t size, unsigned char **result);
    uint16_t checkSum(unsigned char *data, size_t size);
};

#endif //STELLAR_SDK_CPP_MC_KEYPAIR_H