#include "Arduino.h"
#include "Keypair.h"

#include "ed25519/SHA512.h"
#include "ed25519/GE.h"
#include "ed25519/SC.h"
#include "ed25519/FE.h"
#include "crc16/crc16.h"

void Keypair::randomSeed() {
    for (int i = 0; i < SEED_SIZE; i++) {
        rawSeed[i] = random(255);
    }
}

void Keypair::create() {
    ge_p3 A;

    sha512(this->rawSeed, 32, this->rawPrivateKey);
    this->rawPrivateKey[0] &= 248;
    this->rawPrivateKey[31] &= 63;
    this->rawPrivateKey[31] |= 64;

    ge_scalarmult_base(&A, this->rawPrivateKey);
    ge_p3_tobytes(this->rawPublicKey, &A);

    makeSecret();
    makePublicKey();
}

void Keypair::randomKeypair() {
    randomSeed();
    create();
}

void Keypair::sign(const unsigned char *message, size_t message_len) {
    sha512_context hash;
    unsigned char hram[64];
    unsigned char r[64];
    ge_p3 R;


    sha512_init(&hash);
    sha512_update(&hash, this->rawPrivateKey + 32, 32);
    sha512_update(&hash, message, message_len);
    sha512_final(&hash, r);

    sc_reduce(r);
    ge_scalarmult_base(&R, r);
    ge_p3_tobytes(this->rawSignature, &R);

    sha512_init(&hash);
    sha512_update(&hash, this->rawSignature, 32);
    sha512_update(&hash, this->rawPublicKey, 32);
    sha512_update(&hash, message, message_len);
    sha512_final(&hash, hram);

    sc_reduce(hram);
    sc_muladd(this->rawSignature + 32, hram, this->rawPrivateKey, r);
}

int Keypair::constTimeEqual(const unsigned char *x, const unsigned char *y) {
    unsigned char r = 0;

    r = x[0] ^ y[0];
#define FF(i) r |= x[i] ^ y[i]
    FF(1);
    FF(2);
    FF(3);
    FF(4);
    FF(5);
    FF(6);
    FF(7);
    FF(8);
    FF(9);
    FF(10);
    FF(11);
    FF(12);
    FF(13);
    FF(14);
    FF(15);
    FF(16);
    FF(17);
    FF(18);
    FF(19);
    FF(20);
    FF(21);
    FF(22);
    FF(23);
    FF(24);
    FF(25);
    FF(26);
    FF(27);
    FF(28);
    FF(29);
    FF(30);
    FF(31);
#undef FF

    return !r;
}

int Keypair::verify(const unsigned char *message, size_t message_len) {
    unsigned char h[64];
    unsigned char checker[32];
    sha512_context hash;
    ge_p3 A;
    ge_p2 R;

    if (this->rawSignature[63] & 224) {
        return 0;
    }

    if (ge_frombytes_negate_vartime(&A, this->rawPublicKey) != 0) {
        return 0;
    }

    sha512_init(&hash);
    sha512_update(&hash, this->rawSignature, 32);
    sha512_update(&hash, this->rawPublicKey, PUBKEY_SIZE);
    sha512_update(&hash, message, message_len);
    sha512_final(&hash, h);

    sc_reduce(h);
    ge_double_scalarmult_vartime(&R, h, &A, this->rawSignature + 32);
    ge_tobytes(checker, &R);

    if (!constTimeEqual(checker, this->rawSignature)) {
        return 0;
    }

    return 1;
}

void Keypair::addScalar(const unsigned char *scalar) {
    const unsigned char SC_1[32] = {1}; /* scalar with value 1 */

    unsigned char n[32];
    ge_p3 nB;
    ge_p1p1 A_p1p1;
    ge_p3 A;
    ge_p3 public_key_unpacked;
    ge_cached T;

    sha512_context hash;
    unsigned char hashbuf[64];

    int i;

    /* copy the scalar and clear highest bit */
    for (i = 0; i < 31; ++i) {
        n[i] = scalar[i];
    }
    n[31] = scalar[31] & 127;

    /* private key: a = n + t */
    if (this->rawPrivateKey) {
        sc_muladd(this->rawPrivateKey, SC_1, n, this->rawPrivateKey);

        // https://github.com/orlp/ed25519/issues/3
        sha512_init(&hash);
        sha512_update(&hash, this->rawPrivateKey + 32, 32);
        sha512_update(&hash, scalar, 32);
        sha512_final(&hash, hashbuf);
        for (i = 0; i < 32; ++i) {
            this->rawPrivateKey[32 + i] = hashbuf[i];
        }
    }

    /* public key: A = nB + T */
    if (this->rawPublicKey) {
        /* if we know the private key we don't need a point addition, which is faster */
        /* using a "timing attack" you could find out wether or not we know the private
           key, but this information seems rather useless - if this is important pass
           public_key and private_key seperately in 2 function calls */
        if (this->rawPrivateKey) {
            ge_scalarmult_base(&A, this->rawPrivateKey);
        } else {
            /* unpack public key into T */
            ge_frombytes_negate_vartime(&public_key_unpacked, this->rawPublicKey);
            fe_neg(public_key_unpacked.X, public_key_unpacked.X); /* undo negate */
            fe_neg(public_key_unpacked.T, public_key_unpacked.T); /* undo negate */
            ge_p3_to_cached(&T, &public_key_unpacked);

            /* calculate n*B */
            ge_scalarmult_base(&nB, n);

            /* A = n*B + T */
            ge_add(&A_p1p1, &nB, &T);
            ge_p1p1_to_p3(&A, &A_p1p1);
        }

        /* pack public key */
        ge_p3_tobytes(this->rawPublicKey, &A);
    }
}

void Keypair::keyExchange(unsigned char *shared_secret) {
    unsigned char e[32];
    unsigned int i;

    fe x1;
    fe x2;
    fe z2;
    fe x3;
    fe z3;
    fe tmp0;
    fe tmp1;

    int pos;
    unsigned int swap;
    unsigned int b;

    /* copy the private key and make sure it's valid */
    for (i = 0; i < 32; ++i) {
        e[i] = this->rawPrivateKey[i];
    }

    e[0] &= 248;
    e[31] &= 63;
    e[31] |= 64;

    /* unpack the public key and convert edwards to montgomery */
    /* due to CodesInChaos: montgomeryX = (edwardsY + 1)*inverse(1 - edwardsY) mod p */
    fe_frombytes(x1, this->rawPublicKey);
    fe_1(tmp1);
    fe_add(tmp0, x1, tmp1);
    fe_sub(tmp1, tmp1, x1);
    fe_invert(tmp1, tmp1);
    fe_mul(x1, tmp0, tmp1);

    fe_1(x2);
    fe_0(z2);
    fe_copy(x3, x1);
    fe_1(z3);

    swap = 0;
    for (pos = 254; pos >= 0; --pos) {
        b = e[pos / 8] >> (pos & 7);
        b &= 1;
        swap ^= b;
        fe_cswap(x2, x3, swap);
        fe_cswap(z2, z3, swap);
        swap = b;

        /* from montgomery.h */
        fe_sub(tmp0, x3, z3);
        fe_sub(tmp1, x2, z2);
        fe_add(x2, x2, z2);
        fe_add(z2, x3, z3);
        fe_mul(z3, tmp0, x2);
        fe_mul(z2, z2, tmp1);
        fe_sq(tmp0, tmp1);
        fe_sq(tmp1, x2);
        fe_add(x3, z3, z2);
        fe_sub(z2, z3, z2);
        fe_mul(x2, tmp1, tmp0);
        fe_sub(tmp1, tmp1, tmp0);
        fe_sq(z2, z2);
        fe_mul121666(z3, tmp1);
        fe_sq(x3, x3);
        fe_add(tmp0, tmp0, z3);
        fe_mul(z3, x1, z2);
        fe_mul(z2, tmp1, tmp0);
    }

    fe_cswap(x2, x3, swap);
    fe_cswap(z2, z3, swap);

    fe_invert(z2, z2);
    fe_mul(x2, x2, z2);
    fe_tobytes(shared_secret, x2);
}

uint16_t Keypair::checkSum(unsigned char *data, size_t size) {
    return crc16ccitt_xmodem(data, size);
}

void Keypair::encodeCheck(byte versionByte, unsigned char *data, size_t size, unsigned char **result) {
    unsigned char payload[SEED_SIZE+1];
    unsigned char unencoded[SEED_SIZE+3];

    payload[0] = versionByte;
    for (int i = 1 ; i < (SEED_SIZE+1); i++) {
        payload[i] = data[i-1];
    }

    uint16_t chsum = checkSum(payload, sizeof(payload));
    for (int i = 0 ; i < (SEED_SIZE+1); i++) {
        unencoded[i] = payload[i];
    }
    unencoded[SEED_SIZE+1] = chsum & 0xFF;
    unencoded[SEED_SIZE+2] = chsum >> 8;
    int bsize = base32.toBase32(unencoded, sizeof(unencoded), result, true);
}

void Keypair::makeSecret() {
    unsigned char *result = new unsigned char[SECRET_SIZE];
    byte versionByte = STRKEY_SEED_ED25519;
    encodeCheck(versionByte, this->rawSeed, SEED_SIZE, &result);
    memcpy(this->secret, result, SECRET_SIZE);
    free(result);
}

void Keypair::makePublicKey() {
    unsigned char *result = new unsigned char[PUBLIC_SIZE];
    byte versionByte = STRKEY_PUBKEY_ED25519;
    encodeCheck(versionByte, this->rawPublicKey, PUBKEY_SIZE, &result);
    memcpy(this->publicKey, result, PUBLIC_SIZE);
    this->publicKeyStr = String((char*)this->publicKey);
    free(result);
}