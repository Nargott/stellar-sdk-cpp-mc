#include "Stellarsdk.h"
#include "xdr/Stellar-SCP.h"
#include "xdr/Stellar-ledger-entries.h"
#include "xdr/Stellar-ledger.h"
#include "xdr/Stellar-overlay.h"
#include "xdr/Stellar-transaction.h"
#include "xdr/Stellar-types.h"
#include "sha256/Sha256.h"
#include "xdrpp/marshal.h"

#define ED25519_NO_SEED

using namespace stellar;
using namespace stellar::txtest;

StellarSDK::StellarSDK() {

}

void StellarSDK::createPaymentOp(
        Hash const& networkID,
        SequenceNumber seq,
        int64 amount,
        AccountID destination

) {
    Operation op;
    op.body.type(PAYMENT);
    op.body.paymentOp().amount = amount;
    op.body.paymentOp().destination = destination;
    op.body.paymentOp().asset.type(ASSET_TYPE_NATIVE);


    TransactionEnvelope e;
    PublicKey pk;

    memmove(pk.ed25519().data(), keypair.rawSeed + SEED_SIZE, PUBKEY_SIZE);

    e.tx.sourceAccount = pk;
    e.tx.fee = 0;
    e.tx.seqNum = seq;
    e.tx.operations.push_back(op);

//    contentHash = sha256(xdr::xdr_to_opaque(networkID, ENVELOPE_TYPE_TX, e.tx));
    unsigned char* contentHash = (unsigned char*)"zxczxcxzc";

    keypair.sign(contentHash, sizeof(contentHash));
    e.signatures.push_back(keypair.rawSignature);

//    StellarMessage msg;
//    msg.type(TRANSACTION);
//    msg.transaction() = xdr::xdr_to_opaque(e);

//    xdr::xdr_to_opaque(e)
}


//Keypair StellarSDK::GenerateRandomKeypair() {
//    unsigned char *seed;
//    ed25519_create_seed(seed);
//    Keypair keypair = Keypair(seed);
//    ed25519_create_keypair(keypair.public_key, keypair.private_key, keypair.seed);
//    return keypair;
//}