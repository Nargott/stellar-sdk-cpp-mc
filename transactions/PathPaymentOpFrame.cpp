// Copyright 2014 Stellar Development Foundation and contributors. Licensed
// under the Apache License, Version 2.0. See the COPYING file at the root
// of this distribution or at http://www.apache.org/licenses/LICENSE-2.0

#include "util/asio.h"
#include "transactions/CreateAccountOpFrame.h"
#include "transactions/PathPaymentOpFrame.h"
#include "database/Database.h"
#include "ledger/LedgerDelta.h"
#include "ledger/OfferFrame.h"
#include "ledger/TrustFrame.h"
#include "util/Logging.h"
#include <algorithm>

#include "main/Application.h"
#include "medida/meter.h"
#include "medida/metrics_registry.h"

namespace stellar
{

using namespace std;
using xdr::operator==;

PathPaymentOpFrame::PathPaymentOpFrame(Operation const& op,
                                       OperationResult& res,
                                       TransactionFrame& parentTx)
    : OperationFrame(op, res, parentTx)
    , mPathPayment(mOperation.body.pathPaymentOp())
{
}
    bool
    PathPaymentOpFrame::createDestination(Application& app, LedgerManager& ledgerManager, LedgerDelta& delta) {
        // build a createAccountOp
        Operation op;
        op.sourceAccount = mOperation.sourceAccount;
        op.body.type(CREATE_ACCOUNT);
        CreateAccountOp& caOp = op.body.createAccountOp();
        caOp.destination = mPathPayment.destination;
        caOp.startingBalance = mPathPayment.destAmount;
        caOp.body.accountType(ACCOUNT_ANONYMOUS_USER);
        
        OperationResult opRes;
        opRes.code(opINNER);
        opRes.tr().type(CREATE_ACCOUNT);
        
        CreateAccountOpFrame createAccount(op, opRes, mParentTx);
        createAccount.setSourceAccountPtr(mSourceAccount);
        
        // create account
        if (!createAccount.doCheckValid(app) ||
            !createAccount.doApply(app, delta, ledgerManager))
        {
            if (createAccount.getResultCode() != opINNER)
            {
                throw std::runtime_error("Unexpected error code from createAccount");
            }
            PathPaymentResultCode res;
            
            switch (CreateAccountOpFrame::getInnerCode(createAccount.getResult()))
            {
                case CREATE_ACCOUNT_UNDERFUNDED:
                    app.getMetrics().NewMeter({ "op-path-payment", "failure", "underfunded-op" },
                                              "operation").Mark();
                    res = PATH_PAYMENT_UNDERFUNDED;
                    break;
                case CREATE_ACCOUNT_LOW_RESERVE:
                    app.getMetrics().NewMeter({ "op-path-payment", "failure", "low-reserve-op" },
                                              "operation").Mark();
                    res = PATH_PAYMENT_UNDERFUNDED;
                    break;
                case CREATE_ACCOUNT_NOT_AUTHORIZED_TYPE:
                    res = PATH_PAYMENT_NOT_AUTHORIZED;
                    break;
                case CREATE_ACCOUNT_MALFORMED:
                    app.getMetrics().NewMeter({ "op-path-payment", "failure", "malformed-create-account-op" },
                                              "operation").Mark();
                    throw std::runtime_error("Failed to create account - create account op is malformed");
                case CREATE_ACCOUNT_ALREADY_EXIST:
                    app.getMetrics().NewMeter({ "op-path-payment", "failure", "already-exists-create-account-op" },
                                              "operation").Mark();
                    throw std::runtime_error("Failed to create account - already exists");
                case CREATE_ACCOUNT_WRONG_TYPE:
                    app.getMetrics().NewMeter({ "op-path-payment", "failure", "wrong-type-create-account-op" },
                                              "operation").Mark();
                    throw std::runtime_error("Failed to create account - wrong type");
                default:
                    throw std::runtime_error("Unexpected error code from createAccount");
            }
            innerResult().code(res);
            return false;
        }
        innerResult().code(PATH_PAYMENT_SUCCESS);
        app.getMetrics()
        .NewMeter({"op-path-payment", "success", "createDestination"}, "operation")
        .Mark();

        return true;
    }
    

bool
PathPaymentOpFrame::doApply(Application& app, LedgerDelta& delta,
                            LedgerManager& ledgerManager)
{
    Database& db = ledgerManager.getDatabase();

    innerResult().code(PATH_PAYMENT_SUCCESS);

    // tracks the last amount that was traded
    int64_t curBReceived = mPathPayment.destAmount;
    Asset curB = mPathPayment.destAsset;

    // update balances, walks backwards

    // build the full path to the destination, starting with sendAsset
    std::vector<Asset> fullPath;
    fullPath.emplace_back(mPathPayment.sendAsset);
    fullPath.insert(fullPath.end(), mPathPayment.path.begin(),
                    mPathPayment.path.end());

    bool bypassIssuerCheck = false;

    // if the payment doesn't involve intermediate accounts
    // and the destination is the issuer we don't bother
    // checking if the destination account even exist
    // so that it's always possible to send credits back to its issuer
    bypassIssuerCheck = (curB.type() != ASSET_TYPE_NATIVE) &&
                        (fullPath.size() == 1) &&
                        (mPathPayment.sendAsset == mPathPayment.destAsset) &&
                        (getIssuer(curB) == mPathPayment.destination);

    AccountFrame::pointer destination;

    if (!bypassIssuerCheck)
    {
        destination =
            AccountFrame::loadAccount(delta, mPathPayment.destination, db);
        if (!destination)
        {
            if (!createDestination(app, ledgerManager, delta))
            {
                app.getMetrics().NewMeter({ "op-path-payment", "failure", "no-destination" },
                                          "operation").Mark();
                innerResult().code(PATH_PAYMENT_NO_DESTINATION);
                return false;
            }
            app.getMetrics()
            .NewMeter({"op-path-payment", "success", "apply"}, "operation")
            .Mark();
            return true;
        }
    }

    // update last balance in the chain
    if (curB.type() == ASSET_TYPE_NATIVE)
    {
        destination->getAccount().balance += curBReceived;
        destination->storeChange(delta, db);
    }
    
    innerResult().success().last =
        SimplePaymentResult(mPathPayment.destination, curB, curBReceived);

    // last step: we've reached the first account in the chain, update its
    // balance

    int64_t curBSent;

    curBSent = curBReceived;

    if (curBSent > mPathPayment.sendMax)
    { // make sure not over the max
        app.getMetrics()
            .NewMeter({"op-path-payment", "failure", "over-send-max"},
                      "operation")
            .Mark();
        innerResult().code(PATH_PAYMENT_OVER_SENDMAX);
        return false;
    }

    if (curB.type() == ASSET_TYPE_NATIVE)
    {
        auto sourceAccount = mSourceAccount;

        if (ledgerManager.getCurrentLedgerVersion() > 7)
        {
            sourceAccount =
                AccountFrame::loadAccount(delta, mSourceAccount->getID(), db);

            if (!sourceAccount)
            {
                app.getMetrics()
                    .NewMeter({"op-path-payment", "invalid", "no-account"},
                              "operation")
                    .Mark();
                innerResult().code(PATH_PAYMENT_MALFORMED);
                return false;
            }
        }

        int64_t minBalance = sourceAccount->getMinimumBalance(ledgerManager);

        if ((sourceAccount->getAccount().balance - curBSent) < minBalance)
        { // they don't have enough to send
            app.getMetrics()
                .NewMeter({"op-path-payment", "failure", "underfunded"},
                          "operation")
                .Mark();
            innerResult().code(PATH_PAYMENT_UNDERFUNDED);
            return false;
        }

        sourceAccount->getAccount().balance -= curBSent;
        sourceAccount->storeChange(delta, db);
    }
    
    app.getMetrics()
        .NewMeter({"op-path-payment", "success", "apply"}, "operation")
        .Mark();

    return true;
}

bool
PathPaymentOpFrame::doCheckValid(Application& app)
{
    if (mPathPayment.destAmount <= 0 || mPathPayment.sendMax <= 0)
    {
        app.getMetrics()
            .NewMeter({"op-path-payment", "invalid", "malformed-amounts"},
                      "operation")
            .Mark();
        innerResult().code(PATH_PAYMENT_MALFORMED);
        return false;
    }
    if (!isAssetNative(mPathPayment.sendAsset) ||
        !isAssetNative(mPathPayment.destAsset))
    {
        app.getMetrics()
            .NewMeter({"op-path-payment", "invalid", "malformed-currencies"},
                      "operation")
            .Mark();
        innerResult().code(PATH_PAYMENT_MALFORMED);
        return false;
    }
    auto const& p = mPathPayment.path;
    if (!std::all_of(p.begin(), p.end(), isAssetNative))
    {
        app.getMetrics()
            .NewMeter({"op-path-payment", "invalid", "malformed-currencies"},
                      "operation")
            .Mark();
        innerResult().code(PATH_PAYMENT_MALFORMED);
        return false;
    }
    return true;
}
}
