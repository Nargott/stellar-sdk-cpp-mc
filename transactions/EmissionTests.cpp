// Copyright 2014 Stellar Development Foundation and contributors. Licensed
// under the Apache License, Version 2.0. See the COPYING file at the root
// of this distribution or at http://www.apache.org/licenses/LICENSE-2.0
#include "database/Database.h"
#include "ledger/LedgerDelta.h"
#include "ledger/LedgerManager.h"
#include "lib/catch.hpp"
#include "main/Application.h"
#include "main/Config.h"
#include "overlay/LoopbackPeer.h"
#include "test/TestAccount.h"
#include "test/TestExceptions.h"
#include "test/TestUtils.h"
#include "test/TxTests.h"
#include "test/test.h"
#include "transactions/EmissionOpFrame.h"
#include "util/Logging.h"
#include "util/Timer.h"
#include "util/make_unique.h"

using namespace stellar;
using namespace stellar::txtest;

typedef std::unique_ptr<Application> appPtr;

// *XLM emission
TEST_CASE("emission", "[tx][emission]")
{
    Config const& cfg = getTestConfig();

    VirtualClock clock;
    ApplicationEditableVersion app(clock, cfg);
    app.start();

    // set up world
    auto root = TestAccount::createRoot(app);

    Asset xlmCur;

    int64_t txfee = app.getLedgerManager().getTxFee();

    // minimum balance necessary to hold 2 trust lines
    const int64_t minBalance2 =
        app.getLedgerManager().getMinBalance(2) + 10 * txfee;

    // minimum balance necessary to hold 2 trust lines and an offer
    const int64_t minBalance3 =
        app.getLedgerManager().getMinBalance(3) + 10 * txfee;

    const int64_t paymentAmount = minBalance2 > 0 ? minBalance2 : 1000;
    
    AccountFrame::pointer a1Account, rootAccount, a2Account;
    rootAccount = loadAccount(root, app);
    
    int64_t amountBefore = rootAccount->getBalance();
    int64_t totalCoinsBefore = app.getLedgerManager().getCurrentLedgerHeader().totalCoins;
    int64_t totalFeeBefore = app.getLedgerManager().getCurrentLedgerHeader().feePool;
    // create an account
    auto a1 = root.emission("A", paymentAmount);

//    const int64_t morePayment = paymentAmount / 2;
//
//    // sets up gateway account
//    const int64_t gatewayPayment = minBalance2 + morePayment;
//    auto gateway = root.create("gate", gatewayPayment);
//
//    // sets up gateway2 account
//    auto gateway2 = root.create("gate2", gatewayPayment);
//
//    Asset idrCur = makeAsset(gateway, "IDR");
//    Asset usdCur = makeAsset(gateway2, "USD");

    a1Account = loadAccount(a1, app);
    REQUIRE(rootAccount->getMasterWeight() == 1);
    REQUIRE(rootAccount->getHighThreshold() == 0);
    REQUIRE(rootAccount->getLowThreshold() == 0);
    REQUIRE(rootAccount->getMediumThreshold() == 0);
    REQUIRE(a1Account->getBalance() == paymentAmount);
    REQUIRE(a1Account->getMasterWeight() == 1);
    REQUIRE(a1Account->getHighThreshold() == 0);
    REQUIRE(a1Account->getLowThreshold() == 0);
    REQUIRE(a1Account->getMediumThreshold() == 0);
    // root did 2 transactions at this point
    REQUIRE(rootAccount->getBalance() == amountBefore);
    REQUIRE(app.getLedgerManager().getCurrentLedgerHeader().totalCoins == totalCoinsBefore + paymentAmount);
    REQUIRE(app.getLedgerManager().getCurrentLedgerHeader().feePool == totalFeeBefore);

    SECTION("second emission on the same account")
    {
        auto a2 = root.emission("A", paymentAmount);
        a2Account = loadAccount(a2, app);
        REQUIRE(a2Account->getBalance() == paymentAmount * 2);
        // root did 2 transactions at this point
        REQUIRE(rootAccount->getBalance() == amountBefore);
        REQUIRE(app.getLedgerManager().getCurrentLedgerHeader().totalCoins == totalCoinsBefore + paymentAmount * 2);
        REQUIRE(app.getLedgerManager().getCurrentLedgerHeader().feePool == totalFeeBefore);
    }
    SECTION("emission on self")
    {
        auto a2 = root.emission(root, paymentAmount);
        a2Account = loadAccount(a2, app);
        REQUIRE(a2Account->getBalance() == amountBefore + paymentAmount);
        REQUIRE(app.getLedgerManager().getCurrentLedgerHeader().totalCoins == totalCoinsBefore + paymentAmount * 2);
        REQUIRE(app.getLedgerManager().getCurrentLedgerHeader().feePool == totalFeeBefore);
    }
    
}
