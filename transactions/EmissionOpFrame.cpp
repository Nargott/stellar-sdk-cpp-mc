// Copyright 2014 Stellar Development Foundation and contributors. Licensed
// under the Apache License, Version 2.0. See the COPYING file at the root
// of this distribution or at http://www.apache.org/licenses/LICENSE-2.0

#include "util/asio.h"
#include "transactions/EmissionOpFrame.h"
#include "database/Database.h"
#include "ledger/LedgerDelta.h"
#include "ledger/OfferFrame.h"
#include "ledger/TrustFrame.h"
#include "main/Application.h"
#include "medida/meter.h"
#include "medida/metrics_registry.h"
#include "transactions/PathPaymentOpFrame.h"
#include "util/Logging.h"
#include <algorithm>

namespace stellar
{

using namespace std;
using xdr::operator==;

EmissionOpFrame::EmissionOpFrame(Operation const& op, OperationResult& res,
                               TransactionFrame& parentTx)
    : OperationFrame(op, res, parentTx), mEmission(mOperation.body.emissionOp())
{
}

bool
EmissionOpFrame::doApply(Application& app, LedgerDelta& delta,
                        LedgerManager& ledgerManager)
{
    // allow the operation only from the master and only signed by an emission role
    
    if (!(getSourceID() == app.getConfig().MASTER_KEY)) {
        app.getMetrics().NewMeter({ "op-emission", "invalid", "master-is-not-source" },
                                  "operation").Mark();
        innerResult().code(EMISSION_SRC_NOT_AUTHORIZED);
        return false;
    }
    //todo: check whether the signer is admin/emissioner
    
    // make an emission on the master account
    Database& db = ledgerManager.getDatabase();
    LedgerDelta emissionDelta(delta);
    
    auto& lcl = emissionDelta.getHeader();
    
    AccountFrame::pointer master =
        AccountFrame::loadAccount(delta, getSourceID(), db);
    master->getAccount().balance += mEmission.amount;
    if (lcl.totalCoins + mEmission.amount < lcl.totalCoins) {
        app.getMetrics().NewMeter({ "op-emission", "invalid", "total-coins-overflow" },
                                  "operation").Mark();
        innerResult().code(EMISSION_TOTAL_EMISSION_FULL);
        return false;
    }
    
    lcl.totalCoins += mEmission.amount;
    
    master->storeChange(delta, db);
    // build a pathPaymentOp
    Operation op;
    op.sourceAccount = mOperation.sourceAccount;
    op.body.type(PATH_PAYMENT);
    PathPaymentOp& ppOp = op.body.pathPaymentOp();
    ppOp.sendAsset.type(ASSET_TYPE_NATIVE);
    ppOp.destAsset.type(ASSET_TYPE_NATIVE);

    ppOp.destAmount = mEmission.amount;
    ppOp.sendMax = mEmission.amount;

    ppOp.destination = mEmission.destination;

    OperationResult opRes;
    opRes.code(opINNER);
    opRes.tr().type(PATH_PAYMENT);
    PathPaymentOpFrame ppayment(op, opRes, mParentTx);
    ppayment.setSourceAccountPtr(master);

    if (!ppayment.doCheckValid(app) ||
        !ppayment.doApply(app, delta, ledgerManager))
    {
        if (ppayment.getResultCode() != opINNER)
        {
            throw std::runtime_error("Unexpected error code from pathPayment");
        }
        EmissionResultCode res;

        switch (PathPaymentOpFrame::getInnerCode(ppayment.getResult()))
        {
            case PATH_PAYMENT_SRC_NO_TRUST:
            case PATH_PAYMENT_UNDERFUNDED:
            case PATH_PAYMENT_NO_TRUST:
            case PATH_PAYMENT_NO_DESTINATION:
            case PATH_PAYMENT_NOT_AUTHORIZED:
            case PATH_PAYMENT_LINE_FULL:
            case PATH_PAYMENT_NO_ISSUER:
                app.getMetrics()
                .NewMeter({"op-emission", "failure", "Unexpected-error-code"}, "operation")
                .Mark();
            throw std::runtime_error("Unexpected error code from pathPayment");
            break;
        case PATH_PAYMENT_SRC_NOT_AUTHORIZED:
            app.getMetrics()
                .NewMeter({"op-emission", "failure", "src-not-authorized"},
                          "operation")
                .Mark();
            res = EMISSION_SRC_NOT_AUTHORIZED;
            break;
            case PATH_PAYMENT_OVER_SENDMAX:
                app.getMetrics()
                .NewMeter({"op-emission", "failure", "over-send-max"},
                          "operation")
                .Mark();
                res = EMISSION_DEST_FULL;
                break;
                
        default:
            throw std::runtime_error("Unexpected error code from pathPayment");
        }
        innerResult().code(res);
        return false;
    }

    assert(PathPaymentOpFrame::getInnerCode(ppayment.getResult()) ==
           PATH_PAYMENT_SUCCESS);
    
    emissionDelta.commit();
    app.getMetrics()
        .NewMeter({"op-emission", "success", "apply"}, "operation")
        .Mark();
    innerResult().code(EMISSION_SUCCESS);

    return true;
}

bool
EmissionOpFrame::doCheckValid(Application& app)
{
    if (mEmission.amount <= 0)
    {
        app.getMetrics()
            .NewMeter({"op-emission", "invalid", "malformed-negative-amount"},
                      "operation")
            .Mark();
        innerResult().code(EMISSION_MALFORMED);
        return false;
    }
    return true;
}
}
