// Copyright 2014 Stellar Development Foundation and contributors. Licensed
// under the Apache License, Version 2.0. See the COPYING file at the root
// of this distribution or at http://www.apache.org/licenses/LICENSE-2.0

#include "util/asio.h"
#include "transactions/SettlementOpFrame.h"
#include "database/Database.h"
#include "ledger/LedgerDelta.h"
#include "ledger/OfferFrame.h"
#include "ledger/TrustFrame.h"
#include "main/Application.h"
#include "medida/meter.h"
#include "medida/metrics_registry.h"
#include "transactions/PathPaymentOpFrame.h"
#include "util/Logging.h"
#include <algorithm>

namespace stellar
{

using namespace std;
using xdr::operator==;

SettlementOpFrame::SettlementOpFrame(Operation const& op, OperationResult& res,
                               TransactionFrame& parentTx)
    : OperationFrame(op, res, parentTx), mSettlement(mOperation.body.settlementOp())
{
}

bool
SettlementOpFrame::doApply(Application& app, LedgerDelta& delta,
                        LedgerManager& ledgerManager)
{
    // allow the operation only from the master and only signed by a settlement role
    
    if (!(getSourceID() == app.getConfig().MASTER_KEY)) {
        app.getMetrics().NewMeter({ "op-settlement", "invalid", "master-is-not-source" },
                                  "operation").Mark();
        innerResult().code(SETTLEMENT_SRC_NOT_AUTHORIZED);
        return false;
    }
    //todo: check whether the signer is admin/settlement agent
    
    // make a settlement from the master account
    Database& db = ledgerManager.getDatabase();
    LedgerDelta settlementDelta(delta);
    
    auto& lcl = settlementDelta.getHeader();
    
    AccountFrame::pointer master =
        AccountFrame::loadAccount(delta, getSourceID(), db);
    int64_t minBalance = master->getMinimumBalance(ledgerManager);
    
    if ((master->getAccount().balance - mSettlement.amount) < minBalance)
    { // they don't have enough to send
        app.getMetrics()
        .NewMeter({"op-settlement", "failure", "underfunded"},
                  "operation")
        .Mark();
        innerResult().code(SETTLEMENT_UNDERFUNDED);
        return false;
    }
    master->getAccount().balance -= mSettlement.amount;
    lcl.totalCoins -= mSettlement.amount;
    
    master->storeChange(delta, db);
    settlementDelta.commit();
    app.getMetrics()
        .NewMeter({"op-settlement", "success", "apply"}, "operation")
        .Mark();
    innerResult().code(SETTLEMENT_SUCCESS);

    return true;
}

bool
SettlementOpFrame::doCheckValid(Application& app)
{
    if (mSettlement.amount <= 0)
    {
        app.getMetrics()
            .NewMeter({"op-settlement", "invalid", "malformed-negative-amount"},
                      "operation")
            .Mark();
        innerResult().code(SETTLEMENT_MALFORMED);
        return false;
    }
    return true;
}
}
