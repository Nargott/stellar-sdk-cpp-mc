// Copyright 2014 Stellar Development Foundation and contributors. Licensed
// under the Apache License, Version 2.0. See the COPYING file at the root
// of this distribution or at http://www.apache.org/licenses/LICENSE-2.0
#include "database/Database.h"
#include "ledger/LedgerDelta.h"
#include "ledger/LedgerManager.h"
#include "lib/catch.hpp"
#include "main/Application.h"
#include "main/Config.h"
#include "overlay/LoopbackPeer.h"
#include "test/TestAccount.h"
#include "test/TestExceptions.h"
#include "test/TestUtils.h"
#include "test/TxTests.h"
#include "test/test.h"
#include "transactions/MergeOpFrame.h"
#include "transactions/PaymentOpFrame.h"
#include "util/Logging.h"
#include "util/Timer.h"
#include "util/make_unique.h"

using namespace stellar;
using namespace stellar::txtest;

typedef std::unique_ptr<Application> appPtr;



// *XLM Payment
// *Credit Payment
// XLM -> Credit Payment
// Credit -> XLM Payment
// Credit -> XLM -> Credit Payment
// Credit -> Credit -> Credit -> Credit Payment
// path payment where there isn't enough in the path
// path payment with a transfer rate
TEST_CASE("payment", "[tx][payment]")
{
    Config const& cfg = getTestConfig();

    VirtualClock clock;
    ApplicationEditableVersion app(clock, cfg);
    app.start();

    // set up world
    auto root = TestAccount::createRoot(app);

    Asset xlmCur;

    int64_t txfee = app.getLedgerManager().getTxFee();

    // minimum balance necessary to hold 2 trust lines
    const int64_t minBalance2 =
        app.getLedgerManager().getMinBalance(2) + 10 * txfee;

    // minimum balance necessary to hold 2 trust lines and an offer
    const int64_t minBalance3 =
        app.getLedgerManager().getMinBalance(3) + 10 * txfee;

    const int64_t paymentAmount = minBalance2 > 0 ? minBalance2 : 1000;

    // create an account
    auto tmp = root.emission(root, 1000000000000000000 - 2 * app.getConfig().DESIRED_BASE_RESERVE);
    auto a1 = root.create("A", paymentAmount);
    
    const int64_t morePayment = paymentAmount / 2;

    const int64_t assetMultiplier = 10000000;

    int64_t trustLineLimit = INT64_MAX;

    int64_t trustLineStartingBalance = 20000 * assetMultiplier;

    // sets up gateway account
    const int64_t gatewayPayment = minBalance2 + morePayment;
    auto gateway = root.create("gate", gatewayPayment);

    // sets up gateway2 account
    auto gateway2 = root.create("gate2", gatewayPayment);

    Asset idrCur = makeAsset(gateway, "IDR");
    Asset usdCur = makeAsset(gateway2, "USD");

    AccountFrame::pointer a1Account, rootAccount;
    rootAccount = loadAccount(root, app);
    a1Account = loadAccount(a1, app);
    REQUIRE(rootAccount->getMasterWeight() == 1);
    REQUIRE(rootAccount->getHighThreshold() == 0);
    REQUIRE(rootAccount->getLowThreshold() == 0);
    REQUIRE(rootAccount->getMediumThreshold() == 0);
    REQUIRE(a1Account->getBalance() == paymentAmount);
    REQUIRE(a1Account->getMasterWeight() == 1);
    REQUIRE(a1Account->getHighThreshold() == 0);
    REQUIRE(a1Account->getLowThreshold() == 0);
    REQUIRE(a1Account->getMediumThreshold() == 0);
    // root did 2 transactions at this point
    REQUIRE(rootAccount->getBalance() == (1000000000000000000 - paymentAmount -
                                          gatewayPayment * 2 - txfee * 3));

    LedgerDelta delta(app.getLedgerManager().getCurrentLedgerHeader(),
                      app.getDatabase());

    SECTION("Create account")
    {
        SECTION("Success")
        {
            for_all_versions(app, [&]{
                auto b1 = root.create("B", app.getLedgerManager().getMinBalance(0));
                SECTION("Account already exists")
                {
                    REQUIRE_THROWS_AS(
                        root.create("B", app.getLedgerManager().getMinBalance(0)),
                        ex_CREATE_ACCOUNT_ALREADY_EXIST);
                }
            });
        }
        SECTION("Not enough funds (source)")
        {
            for_all_versions(app, [&]{
                REQUIRE_THROWS_AS(gateway.create("B", gatewayPayment * 2 + 1),
                                ex_CREATE_ACCOUNT_UNDERFUNDED);
            });
        }
        SECTION("Amount too small to create account")
        {
            if (app.getLedgerManager().getMinBalance(0) > 0)
            {
                for_all_versions(app, [&]{
                    REQUIRE_THROWS_AS(
                                      root.create("B", app.getLedgerManager().getMinBalance(0) - 1),
                                      ex_CREATE_ACCOUNT_LOW_RESERVE);
                });
            }
        }
    }

    SECTION("a pays b, then a merge into b")
    {
        auto payment = 1000000;
        auto amount = app.getLedgerManager().getMinBalance(0) + payment;
        auto b1 = root.create("B", amount);

        int64 a1Balance = a1.getBalance();
        int64 b1Balance = b1.getBalance();

        auto txFrame = a1.tx(
            {createPaymentOp(b1, 200), createMergeOp(b1)}, app);

        for_all_versions(app, [&]{
            auto res = applyCheck(txFrame, delta, app);

            REQUIRE(txFrame->getResultCode() == txSUCCESS);

            REQUIRE(!loadAccount(a1, app, false));
            REQUIRE((a1Balance + b1Balance - txFrame->getFee()) ==
                    b1.getBalance());
        });
    }

    SECTION("a pays b, then b merge into a")
    {
        auto payment = 1000000;
        auto amount = app.getLedgerManager().getMinBalance(0) + payment;
        auto b1 = root.create("B", amount);

        int64 a1Balance = a1.getBalance();
        int64 b1Balance = b1.getBalance();

        auto txFrame = a1.tx({createPaymentOp(b1, 200),
                              b1.op(createMergeOp(a1))}, app);
        txFrame->addSignature(b1);

        for_all_versions(app, [&]{
            auto res = applyCheck(txFrame, delta, app);

            REQUIRE(txFrame->getResultCode() == txSUCCESS);

            REQUIRE(!loadAccount(b1, app, false));
            REQUIRE((a1Balance + b1Balance - txFrame->getFee()) ==
                    a1.getBalance());
        });
    }

    SECTION("merge then send")
    {
        auto b1 = root.create("B", app.getLedgerManager().getMinBalance(0));

        int64 a1Balance = a1.getBalance();
        int64 b1Balance = b1.getBalance();

        auto txFrame = a1.tx(
            {createMergeOp(b1), createPaymentOp(b1, 200)}, app);

        for_versions_to(7, app, [&]{
            auto res = applyCheck(txFrame, delta, app);

            REQUIRE(txFrame->getResultCode() == txINTERNAL_ERROR);

            REQUIRE(b1Balance == b1.getBalance());
            REQUIRE((a1Balance - txFrame->getFee()) == a1.getBalance());
        });

        for_versions_from(8, app, [&]{
            auto res = applyCheck(txFrame, delta, app);

            REQUIRE(txFrame->getResultCode() == txFAILED);

            REQUIRE(b1Balance == b1.getBalance());
            REQUIRE((a1Balance - txFrame->getFee()) == a1.getBalance());
        });
    }

    SECTION("send XLM to an existing account")
    {
        for_all_versions(app, [&]{
            root.pay(a1, morePayment);

            AccountFrame::pointer a1Account2, rootAccount2;
            rootAccount2 = loadAccount(root, app);
            a1Account2 = loadAccount(a1, app);
            REQUIRE(a1Account2->getBalance() ==
                    a1Account->getBalance() + morePayment);

            // root did 2 transactions at this point
            REQUIRE(rootAccount2->getBalance() ==
                    (rootAccount->getBalance() - morePayment - txfee));
        });
    }

    SECTION("send XLM to a new account (creates destination)")
    {
        for_all_versions(app, [&]{
            auto newAcc = getAccount("B");
            auto paymentAmount = app.getLedgerManager().getCurrentLedgerHeader().baseReserve * 2 + 1;
            root.pay(newAcc, paymentAmount);

            AccountFrame::pointer rootAccount2, newAcc2;
            rootAccount2 = loadAccount(root, app);
            newAcc2 = loadAccount(newAcc, app);
            REQUIRE(rootAccount2->getBalance() ==
                    (rootAccount->getBalance() - txfee - paymentAmount));
            REQUIRE(newAcc2);
            REQUIRE(newAcc2->getBalance() == paymentAmount);

        });
    }

    SECTION("rescue account (was below reserve)")
    {
        for_all_versions(app, [&]{
            int64 orgReserve = app.getLedgerManager().getMinBalance(0);

            auto b1 = root.create("B", orgReserve + 1000);

            // raise the reserve
            uint32 addReserve = 100000;
            app.getLedgerManager().getCurrentLedgerHeader().baseReserve +=
                addReserve;

            // verify that the account can't do anything
            auto tx = b1.tx({createPaymentOp(root, 1)}, app);
            REQUIRE(!applyCheck(tx, delta, app));
            REQUIRE(tx->getResultCode() == txINSUFFICIENT_BALANCE);

            // top up the account to unblock it
            int64 topUp = app.getLedgerManager().getMinBalance(0) - orgReserve;
            root.pay(b1, topUp);

            // payment goes through
            b1.pay(root, 1);
        });
    }

    SECTION("two payments, first breaking second")
    {
        for_all_versions(app, [&]{
            int64 startingBalance = paymentAmount + 5 +
                                    app.getLedgerManager().getMinBalance(0) +
                                    txfee * 2;
            auto b1 = root.create("B", startingBalance);

            auto tx1 = b1.tx({createPaymentOp(root, paymentAmount)}, app);
            auto tx2 = b1.tx({createPaymentOp(root, 6)}, app);

            int64 rootBalance = root.getBalance();
            auto r = closeLedgerOn(app, 2, 1, 1, 2015, {tx1, tx2});
            checkTx(0, r, txSUCCESS);
            if (app.getLedgerManager().getMinBalance(0) > 0)
                checkTx(1, r, txINSUFFICIENT_BALANCE);
            else
                checkTx(1, r, txFAILED);

            int64 expectedrootBalance = rootBalance + paymentAmount;
            int64 expectedb1Balance = app.getLedgerManager().getMinBalance(0) + 5;
            REQUIRE(expectedb1Balance == b1.getBalance());
            REQUIRE(expectedrootBalance == root.getBalance());
        });
    }

    SECTION("pay, merge, create, pay")
    {
        auto amount = 300000000000000;
        auto sourceAccount = root.create("source", amount);
        auto secondSourceAccount = root.create("secondSource", amount);
        auto payAndMergeDestination = root.create("payAndMerge", amount);
        auto balanceBefore = sourceAccount.getBalance()
                + secondSourceAccount.getBalance()
                + payAndMergeDestination.getBalance();

        auto tx = sourceAccount.tx({
            createPaymentOp(payAndMergeDestination, 500000000),
            createMergeOp(payAndMergeDestination),
            secondSourceAccount.op(createCreateAccountOp(sourceAccount, 500000000)),
            createPaymentOp(payAndMergeDestination, 10000000)
        }, app);
        tx->addSignature(secondSourceAccount);
        auto expectedFee = app.getLedgerManager().getTxFee() * tx->getEnvelope().tx.operations.size();
        
        for_versions_to(7, app, [&]{
            REQUIRE(applyCheck(tx, delta, app));
            auto balanceAfter = sourceAccount.getBalance()
                    + secondSourceAccount.getBalance()
                    + payAndMergeDestination.getBalance();
            //todo: WTF?
            REQUIRE(balanceBefore + amount - 1000000000 - expectedFee * 2 == balanceAfter);
        });
        for_versions_from(8, app, [&]{
            REQUIRE(applyCheck(tx, delta, app));
            auto balanceAfter = sourceAccount.getBalance()
                    + secondSourceAccount.getBalance()
                    + payAndMergeDestination.getBalance();
            REQUIRE(balanceBefore - expectedFee == balanceAfter);
        });
    }
}

TEST_CASE("single create account SQL", "[singlesql][paymentsql][hide]")
{
    Config::TestDbMode mode = Config::TESTDB_ON_DISK_SQLITE;
#ifdef USE_POSTGRES
    if (!force_sqlite)
        mode = Config::TESTDB_POSTGRESQL;
#endif

    VirtualClock clock;
    Application::pointer app =
        Application::create(clock, getTestConfig(0, mode));
    app->start();

    auto root = TestAccount::createRoot(*app);
    SecretKey a1 = getAccount("A");
    int64_t txfee = app->getLedgerManager().getTxFee();
    const int64_t paymentAmount =
        app->getLedgerManager().getMinBalance(1) + txfee * 10;

    {
        auto ctx = app->getDatabase().captureAndLogSQL("createAccount");
        auto a1 = root.create("A", paymentAmount);
    }
}
